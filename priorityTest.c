#include "types.h"
#include "stat.h"
#include "user.h"

struct proctime {
  uint creation_time;          // creation time of process
  uint running_time;           // running time of process
  uint sleep_time;             // sleep time of processs
  uint waiting_time;           // waiting time of process
  uint termination_time;       // termination time of process
};

enum policystate
{
  DEFAULT,
  DEFAULT_QUANTUM,
  PRIORITY,
  MQ  
};


int
main(void)
{
    //Change policy to PRIORITY scheduler;
    changePolicy(PRIORITY);

    int pid = 0;
    struct proctime times ={0,0,0,0,0};

    //create child process and sleep for them to finish:
    if((pid = fork()) == 0) {
            changepriority(20000);
            for(int j = 0; j < 50; j++)
                printf(1, "test\n");
            exit();
    }
    else {
        printf(1,"first child has pid: %d | priority: 20000\n", pid);
        if((pid =fork()) == 0) {
            changepriority(10000);
            for(int j = 0; j < 50; j++)
                printf(1, "test\n");
            exit();
        }
        else {
            printf(1,"second child has pid: %d | priority: 10000\n", pid);
            if((pid = fork()) == 0) {
                changepriority(1000);
                for(int j = 0; j < 50; j++)
                    printf(1, "test\n");
                exit();
            }
            else {
                printf(1,"third child has pid: %d | priority: 1000\n", pid);
                if((pid = fork()) == 0) {
                    changepriority(5);
                    for(int j = 0; j < 50; j++)
                        printf(1, "test\n");
                    exit();
                }
                printf(1,"forth child has pid: %d | priority: 5\n", pid);
            }
        }
    }
    printf(1,"----------------\n");
    pid = waitForChild(&times);
    printf(1, "child with pid %d finished at %d\n", pid, times.termination_time);

    pid = waitForChild(&times);
    printf(1, "child with pid %d finished at %d\n", pid, times.termination_time);

    pid = waitForChild(&times);
    printf(1, "child with pid %d finished at %d\n", pid, times.termination_time);

    pid = waitForChild(&times);
    printf(1, "child with pid %d finished at %d\n", pid, times.termination_time);

    //back to default
    changePolicy(DEFAULT);
    exit();        
}
