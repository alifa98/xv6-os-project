#include "types.h"
#include "stat.h"
#include "user.h"

struct proctime {
  uint creation_time;          // creation time of process
  uint running_time;           // running time of process
  uint sleep_time;             // sleep time of processs
  uint waiting_time;           // waiting time of process
  uint termination_time;       // termination time of process
};

enum policystate
{
  DEFAULT,
  DEFAULT_QUANTUM,
  PRIORITY,
  MQ  
};

struct waitproc {
    int quantum_value;
    int waiting_time;
};

int
main(void)
{
    //Change policy to default with Quantum;
    changePolicy(DEFAULT_QUANTUM);
    //initialize quantum value:
    int quantum_values[] = {1, 2, 5, 10, 20, 50, 75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325, 350, 375, 400, 500, 600};
    int current_quantum_value;
    //initialize array of results:
    struct waitproc result[23];

    float average_waiting_time;
    struct proctime times ={0,0,0,0,0};
    //create child process and wait for them to finish:
    for(int i = 0; i < 22; i++){
        average_waiting_time = 0;
        current_quantum_value = quantum_values[i];
        changeQuantum(current_quantum_value);

        if(fork() == 0) {
            for(int j = 0; j < 100; j++)
                printf(1,"Test\n");
            exit();
        }

        else {
            if(fork() == 0) {
                for(int j = 0; j < 100; j++)
                    printf(1,"Test\n");
                exit();
            }
            else {
                if(fork() == 0) {
                    for(int j = 0; j < 100; j++)
                        printf(1,"Test\n");
                    exit();
                }
                else {
                    if(fork() == 0) {
                        for(int j = 0; j < 100; j++)
                            printf(1,"Test\n");
                        exit();
                    }
                }
            }
        }
        waitForChild(&times);
        average_waiting_time += times.waiting_time;
       

        waitForChild(&times);
        average_waiting_time += times.waiting_time;

        waitForChild(&times);
        average_waiting_time += times.waiting_time;

        waitForChild(&times);
        average_waiting_time += times.waiting_time;

        average_waiting_time /= 4;

        result[i].quantum_value = current_quantum_value;
        result[i].waiting_time = average_waiting_time;
    }

    //print results:
    for(int i = 0; i < 22; i++){
        printf(1,"quantum: %d | wait: %d\n",result[i].quantum_value, (int)result[i].waiting_time);
    }

    //back to default
    changePolicy(DEFAULT);
    exit();        
}
