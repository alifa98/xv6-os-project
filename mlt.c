#include "types.h"
#include "stat.h"
#include "user.h"

struct proctime
{
    uint creation_time;    // creation time of process
    uint running_time;     // running time of process
    uint sleep_time;       // sleep time of processs
    uint waiting_time;     // waiting time of process
    uint termination_time; // termination time of process
};

enum policystate
{
    DEFAULT,
    DEFAULT_QUANTUM,
    PRIORITY,
    MQ
};

struct waitproc
{
    int quantum_value;
    int waiting_time;
};

int main(void)
{
    changeQuantum(20);
    changePolicy(MQ);
    if (fork() == 0)
    {
        for(int j = 0; j < 50; j++);
        exit();
    }
    else
    {
        if (fork() == 0)
        {
            for(int j = 0; j < 50; j++);
            exit();
        }
        else
        {
            if (fork() == 0)
            {
                for(int j = 0; j < 50; j++);
                exit();
            }
        }
    }

    struct proctime times = {0, 0, 0, 0, 0};

    waitForChild(&times);
    waitForChild(&times);
    waitForChild(&times);
    changePolicy(DEFAULT);
    printf(1,"finished!\n");
    exit();
}
