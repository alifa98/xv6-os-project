#include "types.h"
#include "x86.h"
#include "defs.h"
#include "date.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"

int
sys_fork(void)
{
  return fork();
}

int
sys_exit(void)
{
  exit();
  return 0;  // not reached
}

int
sys_wait(void)
{
  return wait();
}

int
sys_kill(void)
{
  int pid;

  if(argint(0, &pid) < 0)
    return -1;
  return kill(pid);
}

int
sys_getpid(void)
{
  return myproc()->pid;
}

int
sys_sbrk(void)
{
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;
  addr = myproc()->sz;
  if(growproc(n) < 0)
    return -1;
  return addr;
}

int
sys_sleep(void)
{
  int n;
  uint ticks0;

  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(myproc()->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
  return 0;
}

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
  uint xticks;

  acquire(&tickslock);
  xticks = ticks;
  release(&tickslock);
  return xticks;
}

int
sys_getchildren(void)
{
  char *arg;
  argptr(0, (void*)&arg, sizeof(char) * 129);
  return getchildren(arg);
}

int
sys_changepriority(void){
  int priority_number;
  if (argint(0, &priority_number) < 0)
    return -1;
  return changepriority(priority_number);
}

int
sys_waitForChild(void){
  struct proctime *arg0;
  argptr(0, (void*)&arg0, sizeof(struct proctime));
  return waitForChild(arg0);
}

int sys_changePolicy(void)
{
  int policy;
  if (argint(0, &policy) < 0)
    return -1;
  return changePolicy(policy);
}

int sys_changeQuantum(void)
{
  int quantum;
  if (argint(0, &quantum) < 0)
    return -1;
  return changeQuantum(quantum);
}