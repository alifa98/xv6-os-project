#include "defs.h"

char buffer[128]; //we have max 64 processes in process table

int lenOfString(char* str) {
	int i = 0;
	while (str[i] != '\0') {
		i++;
	}
	return i;
}

int len(int num) {
	int temp = num;
	int counter = 0;
	while (temp != 0){
		temp /= 10;
		counter++;
	}
	return counter;
}

char* intToString(int num) {
    int length = len(num);

	int numArr[length + 1];
	int i,digit,end_of_array = length - 1, temp = num;
	for (i = 0 ; i < length; i++) { //convert num to array
		digit = temp % 10;
		temp /= 10;
		numArr[i] = digit;
	}
	if(length == 1){ //in case our num is 1 digit number
        numArr[length] = 0;
        end_of_array = length;
    }
	static char numString[100];
	for (i = end_of_array; i >= 0; i--){ //convert array to string
		char digitChar = (char)(numArr[i] + 48);
		numString[end_of_array - i] = digitChar;
	}
	numString[end_of_array + 1] = '\0';

	return numString;

}

char* concat(char *str1, char *str2) {
    int len_str1 = lenOfString(str1);
    int len_str2 = lenOfString(str2);

    int i, j;
	for (i = 0; i < len_str1; i++) {
		buffer[i] = str1[i];
	}

	int start_point;
	if(len_str1 == 0 && str2[0] == '0'){
        start_point = 1;
  }
  else {
        start_point = 0;
  }

	for (i = len_str1, j = start_point; i < len_str1 + len_str2; i++, j++) {
		buffer[i] = str2[j];
	}

	return buffer;
}

char* append(char *str, int pid) {
    char* result; //we have max 64 processes in
    char *pid_str = intToString(pid);
    result = concat(str, pid_str);
    return result;
}
void makebufferClean(){
	//make buffer clean:
	int i;
	for (i = 0; i < 128; i++)
		buffer[i] = '\0';
}