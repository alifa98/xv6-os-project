#include "types.h"
#include "stat.h"
#include "user.h"

struct proctime {
  uint creation_time;          // creation time of process
  uint running_time;           // running time of process
  uint sleep_time;             // sleep time of processs
  uint waiting_time;           // waiting time of process
  uint termination_time;       // termination time of process
};
int
main(void)
{   
    if(fork() > 0) {
        struct proctime times ={0,0,0,0,0};
        int pid = waitForChild(&times);
        printf(1, "pid: %d |create: %d|run:%d|sleep:%d|terminate:%d|wait:%d\n",pid, times.creation_time,
        times.running_time, times.sleep_time, times.termination_time, times.waiting_time);
    }
    else {
        printf(1,"started!\n");
        for(int i = 1; i < 100; i++)
            printf(1, "i: %d\n", i);
        printf(1,"Finished!\n");
    }
    exit();
}
